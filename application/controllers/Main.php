<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Main extends CI_Controller {


    public function index()
    {


        $params = array();
        $this->load->library('I18N_Arabic', $params);
        $this->i18n_arabic->load('Glyphs');
        $name = $this->input->post('name');

        $file_name = str_replace(' ','_',$name);
        $file_name = str_replace('/','-',$file_name);
        if( $name ){

            $config['source_image'] = './cards/card1.png';
            $config['new_image'] = './imgs/'.$file_name.'.png';
            $config['wm_text'] = $this->i18n_arabic->utf8Glyphs($name);
            $config['wm_type'] = 'text';
            //$config['wm_font_path'] = './system/fonts/texb.ttf';
            $config['wm_font_path'] = './theme/fonts/DIN_Next_LT_W23_Regular_2.ttf';

            $config['wm_font_size'] = '40';
            $config['wm_font_color'] = 'ccc';
            $config['wm_vrt_alignment'] = 'top';
            $config['wm_hor_alignment'] = 'center';
            //$config['wm_padding'] = '0';
            $config['wm_hor_offset'] = '0';
            $config['wm_vrt_offset'] = '645';

            $this->load->library('image_lib',$config);
            $this->image_lib->initialize($config);

            $this->image_lib->watermark();


            //$file = FCPATH . "imgs/{$file_name}.png";

            //header("Content-Type: image/png");
            //header("Content-Disposition: filename={$file}");
            //header("Content-Disposition: attachment; filename={$file}");
            //ImagePng($im);
            //ImageDestroy($im);
            //return;
            redirect("imgs/{$file_name}.png");

        }

        /*
                $url_para = $_GET['file_name'];
                $job_no = $_GET['jobno'];
                $this->load->helper('download');
                $data = file_get_contents(site_url().'/../images/uploads/'.$job_no."/".$url_para[0]);
                $name = 'myphoto.jpg';

                force_download($name, $data);
        */



        $this->load->view('layout',compact('file_name'));
    }


}
